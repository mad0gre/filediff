#! /usr/bin/env python

from argparse import ArgumentParser


def diff(left, right):
    """
    Calculates the difference between left and right files, indicating original line on left and right files, a flag
    indicating whether the line was kept, removed or added, and the line itself.
    Prints the result to default output.

    :param left: Base file for comparison.
    :param right: File being compared to base.
    """

    left = open(left)
    right = open(right)
    diff_result = diff_lines(left.readlines(), right.readlines())
    for diff_line in diff_result:
        print(diff_line[2], diff_line[3].rstrip())


def diff_lines(left, right):
    """
    Calculates the difference between left and right files, but each file is represented by an iterable of lines.
    Return a list of tuples containing the original line on left iterable, original line on right iterable, diff flag
    (``'+'``, ``'-'`` or ``' '``), and the line in question.

    :param left: Iterable containing the lines of left file.
    :param right: Iterable containing the lines of right file.
    :return: List of tuples containing the left index of line, right index of line, diff flag, and line.
    """
    result = []
    lidx, ridx = 0, 0
    while lidx < len(left) and ridx < len(right):
        lline, rline = left[lidx], right[ridx]
        if lline == rline:
            result.append((lidx, ridx, ' ', lline))
            lidx += 1
            ridx += 1
        else:
            # Check if current line from left appears ahead of current position on right. If so, lines were added.
            # Otherwise left line was removed.
            try:
                new_ridx = right.index(lline, ridx)
                for i in range(ridx, new_ridx):
                    rline = right[i]
                    result.append((-1, i, '+', rline))
                ridx = new_ridx
            except ValueError:
                result.append((lidx, -1, '-', lline))
                lidx += 1

    # Remaining lines on left were removed.
    while lidx < len(left):
        line = left[lidx]
        result.append((lidx, -1, '-', line))
        lidx += 1

    # Remaining lines on right were added.
    while ridx < len(right):
        line = right[ridx]
        result.append((-1, ridx, '+', line))
        ridx += 1

    return result


def _create_arg_parser():
    parser = ArgumentParser(description="Calculate difference in between two files")
    parser.add_argument('left', help="Base file for comparison")
    parser.add_argument('right', help="File being compared to base")
    return parser


def _main():
    parser = _create_arg_parser()
    args = parser.parse_args()
    diff(args.left, args.right)


if __name__ == '__main__':
    _main()
