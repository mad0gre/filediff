# filediff

Simple file diff command line tool.


## Usage
From the project folder:

##### Linux
`./filediff.py file1 file2`

##### Linux or Windows
`python filediff.py file1 file2`


## Requirements

Python 3 is installed on the environment.
